.. bitter-porridge documentation master file, created by
   sphinx-quickstart on Wed Sep  3 10:38:21 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bitter-porridge's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   getting_started
   okay_we_started
   orgdir/forge

Another Topic:

.. toctree::
   :maxdepth: 2

   some_more_stuff
   in_another_topic

My Dir

.. toctree::
   :maxdepth: 2

   mydir/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

